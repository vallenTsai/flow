sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Flow.controller.Folder", {
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf SplitApp.view.Folder
		 */
		onInit: function() {
			var ctrl = this;
			
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			
			$.ajax("/Flow7Api/api/diagram")
				.done(function(data){
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "diagram");
				});
		},

		goToList1: function() {
			this._oRouter.navTo("list1");
		},
		goToList2: function() {
			this._oRouter.navTo("list2");
		},
		
		onPress: function(oEvent){
			//console.log(oEvent);
			var oObject = oEvent.getSource().getBindingContext("diagram");
			//console.log(oObject);
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			this._oRouter.navTo("tablelist", {
				folderguid: oItem.FolderGuid,
				query: {
					test: oItem.FolderGuid
				}
			});
		}
	});

});