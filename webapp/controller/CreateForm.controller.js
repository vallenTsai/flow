sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	'sap/m/MessageToast'
], function(Controller, History, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Flow.controller.CreateForm", {
		onInit: function() {
			var ctrl = this;

			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("Workflow", "APPLICANT_SUBMIT", this.onSumit, this);
		},
		onFormSubmit: function() {
			// 通知 EmbeddedView 表單回傳 oData
			sap.ui.getCore().getEventBus().publish("FORM", "APPLICANT_SUBMIT");
		},
		onSumit: function(sChanel, sEvent, oData) {
			var ctrl = this;
			MessageToast.show("表單送出成功"+JSON.stringify(oData));
		},
		onBackPress: function(oEvent) {
			var ctrl = this;
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);
				oRouter.navTo("initialized");
			}
		}
	});
});