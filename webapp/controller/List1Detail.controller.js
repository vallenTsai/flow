sap.ui.define([
	'jquery.sap.global',
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	'sap/ui/core/Fragment'
], function(jQuery, Controller, History, Fragment) {
	"use strict";

	return Controller.extend("nnext.iq.Flow.controller.List1Detail", {
		onInit: function() {
			var ctrl = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(function(oEvent) {
				var requisitionId = oEvent.getParameter("arguments").requisitionid;
				$.ajax("/Flow7Api/api/fdp/m/"+requisitionId)
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
						ctrl.getView().setModel(oData, "requisitions");
						ctrl.getView().bindElement("requisitions");
					});
			});
		},

		onBackPress: function(oEvent) {
			var oHistory, sPreviousHash;
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				//window.history.go(-1);
			} else {
				//this.getRouter().navTo("initialized", {}, true);
			}
		},
		onCreate: function(oEvent) {
			this._oRouter.navTo("new");
		}
	});

});