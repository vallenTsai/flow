sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Flow.controller.DisplayDetail", {

		onInit: function() {
			var ctrl = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(function(oEvent) {
				var requisitionId = oEvent.getParameter("arguments").requisitionid;
				$.ajax("/Flow7Api/api/fdp/m/"+requisitionId)
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
						ctrl.getView().setModel(oData, "requisitions");
						ctrl.getView().bindElement("requisitions");
					});
			});
		},
		onCreate: function(oEvent) {
			alert("test");
			this._oRouter.navTo("createformXXXX");
		}
	});

});