sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast",
	"nnext/iq/Flow/model/underscore"
], function(Controller, JSONModel, Filter, FilterOperator, MessageToast, underscore) {
	"use strict";

	return Controller.extend("nnext.iq.Flow.controller.Fdp", {
		onInit: function() {
			var ctrl = this;
			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);
			ctrl._oRouter.attachRouteMatched(function(oEvent) {
				$.ajax("/Flow7Api/api/fdp")
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
						ctrl.getView().setModel(oData, "fdp");
					});
			});
			//取收據種類
			new sap.ui.model.json.JSONModel("/ErpApi/api/receipt").attachRequestCompleted(function(oData) {
				ctrl.getView().setModel(oData.getSource(), "receiptAll");

				self.renderReceipt(oView.getModel("fdp").getProperty("/DocumentCategoryId"));
			});
			//取部門及申請人資料
			ctrl.getView().setModel(new JSONModel("model/departments.json"), "departments");
			ctrl.getView().setModel(new JSONModel("model/members.json"), "members");
			//發佈共用送單函數
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("FORM", "APPLICANT_SUBMIT", this.onSumit, this);
		},
		changeDepartment: function(oEvent) {
			var ctrl = this;
			var sDepartmentID = oEvent.getParameters().selectedItem.getKey();
			var aFilter = [];

			//依部門篩選申請人
			aFilter.push(new Filter("departmentId", FilterOperator.Contains, sDepartmentID));
			ctrl.getView().byId("applicantID").getBinding("items").filter(aFilter);
		},
		handleSelectDialogPress: function(oEvent) {
			var ctrl = this;

			if (!ctrl._oDialog) {
				ctrl._oDialog = ctrl.getView().byId("testDialog");
				ctrl._oDialog.setModel(ctrl.getView().getModel());
			}

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", ctrl.getView(), ctrl._oDialog);
			ctrl._oDialog.open();
		},
		onReceiptChange: function(oEvent) {
			this.renderReceipt(parseInt(this.getView().byId("documentcategoryid").getSelectedKey()));
			//this.updateReceiptData();
		},

		renderReceipt: function(nReceiptId) {
			var oView = this.getView();
			var oReceipt = oView.getModel("receiptAll").getProperty("/");

			oView.setModel(new sap.ui.model.json.JSONModel(_.findWhere(oReceipt, {
				"ReceiptId": nReceiptId
			})), "receipt");
		},
		onSumit: function(oEvent) {
			// 收到 submit 通知, 回傳 oData
			var ctrl = this;
			var oModel = ctrl.getView().getModel("fdp");

			sap.ui.getCore().getEventBus().publish("Workflow", "APPLICANT_SUBMIT", oModel.oData);
		}
	});
});